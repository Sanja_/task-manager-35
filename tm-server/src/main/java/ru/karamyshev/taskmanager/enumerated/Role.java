package ru.karamyshev.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

public enum Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
