package ru.karamyshev.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.config.ApplicationConfig;

public class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }
 
}
