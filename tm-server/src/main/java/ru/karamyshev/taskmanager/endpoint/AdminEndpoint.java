package ru.karamyshev.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IAdminEndpoint;
import ru.karamyshev.taskmanager.api.service.IAdminService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.dto.Fail;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.Success;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IAdminService adminService;

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBinary(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBinary(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);

        try {
            adminService.loadBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBinary(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);

        try {
            adminService.clearBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveBinary();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadBase64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearBase64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataFasterJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataFasterJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
       sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataFasterJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearFasterJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearFasterXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJaxBJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJaxBJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJaxBJson(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearJaxBJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result saveDataJaxBXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.saveJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result loadDataJaxBXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.loadJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearDataJaxBXml(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        try {
            adminService.clearJaxBXml();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

}
