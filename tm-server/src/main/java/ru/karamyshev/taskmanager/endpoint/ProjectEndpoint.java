package ru.karamyshev.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IProjectEndpoint;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public void createNameProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        projectService.clearAllByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String name
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String id,
            @WebParam(name = "projectUpName", partName = "projectUpName") @Nullable final String name,
            @WebParam(name = "projectUpDescription", partName = "projectUpDescription") @Nullable String description
    ) throws Exception {
        sessionService.validate(session);
        projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String name
    ) throws Exception {
        sessionService.validate(session);
        projectService.removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String id
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String id
    ) throws Exception {
        sessionService.validate(session);
        projectService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> getProjectList(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projects", partName = "projects") @Nullable final List<Project> projects
    ) throws Exception {
        sessionService.validate(session);
        // projectService.load(projects);
    }

    @Override
    @WebMethod
    public void createProjectAndDescription(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

}
