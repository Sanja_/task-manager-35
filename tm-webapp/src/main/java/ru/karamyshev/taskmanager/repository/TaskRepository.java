package ru.karamyshev.taskmanager.repository;

import org.springframework.stereotype.Repository;
import ru.karamyshev.taskmanager.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE =
            new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("DEMO TASK", "info task DEMO TASK"));
        add(new Task("TEST TASK", "info task TEST TASK"));
        add(new Task("MEGA TASK", "info task MEGA TASK"));
        add(new Task("DAO TASK", "info task DAO TASK"));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public void removeById(String id) {
        tasks.remove(id);
    }

    public Task findById(String id) {
        if (tasks.containsKey(id)) return tasks.get(id);
        return null;
    }

    public void create(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

}
