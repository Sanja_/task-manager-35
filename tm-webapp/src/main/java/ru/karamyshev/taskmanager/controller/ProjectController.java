package ru.karamyshev.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.repository.ProjectRepository;


@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("/projects/projects-list",
                "projects", projectRepository.findAll());
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/create")
    public String create(@ModelAttribute("project") Project project) {
        projectRepository.create(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/create")
    public ModelAndView create(Model model) {
        final Project project = new Project();
        return new ModelAndView("/projects/project-create", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(Project project) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id);
        return new ModelAndView("/projects/project-edit", "project", project);
    }

    @GetMapping("/project/view/{id}")
    public ModelAndView view(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id);
        return new ModelAndView("/projects/project-view", "project", project);
    }

}
