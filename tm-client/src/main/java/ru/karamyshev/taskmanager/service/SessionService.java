package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.ISessionService;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO session;

    @Override
    public void setSession(@NotNull final SessionDTO session) {
        this.session = session;
    }

    @Override
    public void clearSession() {
        session = null;
    }

    @Nullable
    @Override
    public SessionDTO getSession() {
        return session;
    }

}
