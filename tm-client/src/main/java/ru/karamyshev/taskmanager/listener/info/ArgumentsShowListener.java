package ru.karamyshev.taskmanager.listener.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;

import java.util.List;

@Component
public class ArgumentsShowListener extends AbstractListener {

    @Nullable
    @Autowired
    List<AbstractListener> commandsList;

    @NotNull
    @Override
    public String arg() {
        return "-agr";
    }

    @NotNull
    @Override
    public String command() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsShowListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("\n [ARGUMENTS]");
        for (final AbstractListener command : commandsList) System.out.println(command.arg());
    }

}
